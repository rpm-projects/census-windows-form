﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ILP1_censo.Model
{
    public class Answer
    {
        public string dateAnswered { get; set; }
        public string name { get; set; }
        public int age { get; set; }
        public double weigth { get; set; }
        public bool married { get; set; }
        public string preference { get; set; }
        public string movie { get; set; }
        public string interest { get; set; }
        public string food { get; set; }
        public string birthday { get; set; }
        public string gender { get; set; }
        public string about { get; set; }

    }
}
