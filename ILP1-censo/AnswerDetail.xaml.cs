﻿using ILP1_censo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ILP1_censo
{
    /// <summary>
    /// Interaction logic for AnswerDetail.xaml
    /// </summary>
    public partial class AnswerDetail : Window
    {
        public AnswerDetail(Answer answer)
        {
            InitializeComponent();

            label_name.Content = answer.name;
            label_age.Content = answer.age;
            label_weight.Content = answer.weigth;
            label_married.Content = answer.married;
            label_prefer.Content = answer.preference;
            label_movie.Content = answer.movie;
            label_interests.Content = answer.interest;
            label_fav_food.Content = answer.food;
            label_birthday.Content = answer.birthday;
            label_gender.Content = answer.gender;
            label_about.Content = answer.about;
        }
    }
}
