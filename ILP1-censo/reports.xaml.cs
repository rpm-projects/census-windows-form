﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ILP1_censo.Model;
using System.Collections.ObjectModel;

namespace ILP1_censo
{
    /// <summary>
    /// Interaction logic for reports.xaml
    /// </summary>
    public partial class reports : Window
    {
        public reports()
        {
            InitializeComponent();
        }

        private void reportWhere(object sender, RoutedEventArgs e)
        {
            List<Answer> reportList = new List<Answer>();
            IEnumerable<Answer> listResult = reportList;

            //Query by name
            if (textBox_name.Text != "")
            {
                listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => it.name.Contains(textBox_name.Text)));
            }

            /*    if(textBox_age1.Text != "")
                {
                    listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => Convert.ToInt32(it.age) >= Convert.ToInt32(textBox_age1.Text)));
                }

                if(textBox_age2.Text != "")
                {
                    listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => Convert.ToInt32(it.age) <= Convert.ToInt32(textBox_age2.Text)));
                }

                if (textBox_interest.Text != "")
                {
                    listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => it.interest.Contains(textBox_interest.Text)));
                }

                if (textBox_interest.Text != "")
                {
                    listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => it.interest.Contains(textBox_interest.Text)));
                }

                if (textBox_interest.Text != "")
                {
                    listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => it.interest.Contains(textBox_interest.Text)));
                }

                if (textBox_interest.Text != "")
                {
                    listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => it.interest.Contains(textBox_interest.Text)));
                }

                if (comboBox_gender.SelectedValue == "Male")
                {
                    listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => it.interest.Contains("Male")));
                }
                else if(comboBox_gender.SelectedValue == "Female")
                {
                    listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => it.interest.Contains("Female")));
                }

                //Married?
                if (radioButton_yes.IsChecked == true)
                {
                    listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => it.married));
                }
                else if(radioButton_no.IsChecked == false)
                {
                    listResult = listResult.Concat(AnswerManager.listAnswers.Where(it => !it.married));
                }

        */

            reportList = listResult.ToList<Answer>();

            VisualizeReport visuReport = new VisualizeReport(reportList);
            visuReport.Show();
        }

        private void reportOrder(object sender, RoutedEventArgs e)
        {
            List<Answer> reportList = new List<Answer>();
            IEnumerable<Answer> listResult = reportList;
            String fieldValue = comboBox_order.Text;
            

            switch (fieldValue)
            {
                case "Name":
                    reportList = listResult.Concat(AnswerManager.listAnswers.OrderByDescending(it => it.name)).ToList<Answer>();
                    break;
                case "Age":
                    reportList = listResult.Concat(AnswerManager.listAnswers.OrderByDescending(it => it.age)).ToList<Answer>();
                    break;
            }

            VisualizeReport visuReport = new VisualizeReport(reportList);
            visuReport.Show();
        }



        private void reportMax(object sender, RoutedEventArgs e)
        {
            List<Answer> reportList = new List<Answer>();
            IEnumerable<Answer> listResult = reportList;
            String fieldValue = comboBox_max.Text;

            double max = 0;
            Answer item;

            switch (fieldValue)
            {
                case "Weight":
                    max = AnswerManager.listAnswers.Max<Answer>(it => it.weigth);
                    item = AnswerManager.listAnswers.First(x => x.weigth == max);
                    reportList.Add(item);
                    break;
                case "Age":
                    max = AnswerManager.listAnswers.Max<Answer>(it => it.weigth);
                    item = AnswerManager.listAnswers.First(x => x.weigth == max);
                    reportList.Add(item);
                    break;
            }

            VisualizeReport visuReport = new VisualizeReport(reportList);
            visuReport.Show();
        }

        private void reportMin(object sender, RoutedEventArgs e)
        {
            List<Answer> reportList = new List<Answer>();
            IEnumerable<Answer> listResult = reportList;
            String fieldValue = comboBox_min.Text;

            double max = 0;
            Answer item;

            switch (fieldValue)
            {
                case "Weight":
                    max = AnswerManager.listAnswers.Min<Answer>(it => it.weigth);
                    item = AnswerManager.listAnswers.First(x => x.weigth == max);
                    reportList.Add(item);
                    break;
                case "Age":
                    max = AnswerManager.listAnswers.Min<Answer>(it => it.weigth);
                    item = AnswerManager.listAnswers.First(x => x.weigth == max);
                    reportList.Add(item);
                    break;
            }

            VisualizeReport visuReport = new VisualizeReport(reportList);
            visuReport.Show();
        }

        private void reportSelect(object sender, RoutedEventArgs e)
        {

            List<int> listInt = new List<int>();

            foreach (Button item in listBox.SelectedItems)
            {

                System.Diagnostics.Debug.WriteLine(item.Content);

                listInt.Add(Convert.ToInt32(item.Content));
            }

            List<Answer> reportList = new List<Answer>();
            IEnumerable<Answer> listResult = reportList;
            var field = comboBox_min.SelectedValue;

            listResult = from answer in AnswerManager.listAnswers
                                 where listInt.Any(s => s == answer.age)
                                 select answer;

            VisualizeReport visuReport = new VisualizeReport(listResult.ToList<Answer>());
            visuReport.Show();


        }
    }
}
