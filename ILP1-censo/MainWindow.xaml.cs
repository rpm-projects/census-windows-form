﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ILP1_censo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        //Show Quiz Window
        private void btnQuiz_Click(object sender, RoutedEventArgs e)
        {
            Window1 quiz = new Window1();
            quiz.Show(); 

        }

        //Show Menu with Answers already answered
        private void btnListAnswers_Click(object sender, RoutedEventArgs e)
        {
            AnswersMenu answersMenu = new AnswersMenu();
            answersMenu.Show();
        }

        //Show screen to generate reports
        private void button_Click(object sender, RoutedEventArgs e)
        {
            reports reports = new reports();
            reports.Show();
        }
    }
}
