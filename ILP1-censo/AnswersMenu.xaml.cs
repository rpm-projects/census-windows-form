﻿using ILP1_censo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ILP1_censo
{
    /// <summary>
    /// Interaction logic for AnswersMenu.xaml
    /// </summary>
    public partial class AnswersMenu : Window
    {

        Answer answerToShow = null;

        public AnswersMenu()
        {
            InitializeComponent();
            createAnswersMenu();
        }

        public void createAnswersMenu()
        {
            foreach(Answer answer in AnswerManager.listAnswers)
            {
                var stackPanel_2 = new StackPanel { Orientation = Orientation.Vertical };
                stackPanel_2.Children.Add(new Label { Content = answer.dateAnswered });
                Button btnShow = new Button { Content = "Show" };
                btnShow.AddHandler(Button.ClickEvent, new RoutedEventHandler(button_Click));
                btnShow.Tag = answer.dateAnswered;
                stackPanel_2.Children.Add(btnShow);
                stackPanel_2.Orientation = Orientation.Horizontal;
                stackPanel_1.Children.Add(stackPanel_2);
            }
        }

        void button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = sender as Button;
            var chosenAnswer = AnswerManager.listAnswers.Where(x => x.dateAnswered.Equals(btn.Tag as string));
            List<Answer> list = chosenAnswer.ToList<Answer>();

            AnswerDetail answersDetail = new AnswerDetail(list.First<Answer>());
            answersDetail.Show();
        }
    }
}
