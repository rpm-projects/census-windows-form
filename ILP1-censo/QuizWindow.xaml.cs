﻿using ILP1_censo.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ILP1_censo
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        //Save answers
        private void button_save_Click(object sender, RoutedEventArgs e)
        {
            Answer answer = new Answer();

            answer.dateAnswered = DateTime.Now.ToString();
            answer.name = textBox_name.Text;
            answer.age = textBox_age.Text != "" ? Convert.ToInt32(textBox_age.Text) : 0;
            answer.weigth = textBox_weight.Text != "" ? Convert.ToDouble(textBox_weight.Text) : 0;
            answer.married = Convert.ToBoolean(radioButton_no.IsChecked);
            answer.preference = comboBox_prefer.Text;
            StringBuilder interests = new StringBuilder();
            interests.Append(Convert.ToBoolean(checkBox_it.IsChecked) ? checkBox_it.ToString() : "");
            interests.Append(Convert.ToBoolean(checkBox_news.IsChecked) ? checkBox_news.ToString() : "");
            interests.Append(Convert.ToBoolean(checkBox_travel.IsChecked) ? checkBox_travel.ToString() : "");
            interests.Append(Convert.ToBoolean(checkBox_music.IsChecked) ? checkBox_music.ToString() : "");
            answer.interest = interests.ToString();
            answer.food = textBox_fav_food.Text;
            answer.birthday = textBox_birthday.Text;
            answer.gender = comboBox_gender.Text;
            answer.about = textBox_about.Text;

            AnswerManager.listAnswers.Add(answer);

            //Close this window
            this.Close();
        }
    }
}
