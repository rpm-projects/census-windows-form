﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ILP1_censo.Model;

namespace ILP1_censo
{
    /// <summary>
    /// Interaction logic for VisualizeReport.xaml
    /// </summary>
    public partial class VisualizeReport : Window
    {
        public VisualizeReport(List<Answer> listAnswer)
        {
            InitializeComponent();

            foreach (Answer answer in listAnswer)
            {
                var stackPanel_2 = new StackPanel { Orientation = Orientation.Vertical };
                stackPanel_2.Children.Add(new Label { Content = answer.dateAnswered });
                stackPanel_2.Children.Add(new Label { Content = answer.name });
                stackPanel_2.Orientation = Orientation.Horizontal;
                stackPanel_1.Children.Add(stackPanel_2);
            }
        }
    }
}
